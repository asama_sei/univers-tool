require 'csv'
require 'json'
require 'rest-client'
require 'sqlite3'

File.delete('./data.json') if File.exist?('./data.json')
File.delete('./marketable.json') if File.exist?('./marketable.json')
db = SQLite3::Database.new('data.db')

i = CSV.open('Item.csv')
t = Array.new

i.each do |l|
	# id, name, [TradeAble, BuyPrice, SellPrice, universalis] , [A, I0, A0, I1, A1, I2, A2, I3, A3, I4, A4, I5, A5, I6, A6, I7, A7, I8, A8, I9, A9]
	t.push [l[0].to_i, l[1], [(l[23]=='True' ? true:false), l[26].to_i, l[27].to_i, false], Array.new]
end
i.close

db.execute 'DELETE FROM Universalis'

rest_api = RestClient.get('https://universalis.app/api/v2/marketable')
JSON.parse(rest_api.body.to_s).each do |l|
	break if l > t[-1][0]
	t[l][2][3] = true
	db.execute "INSERT INTO Universalis (ID, UpdateTime, lastUploadTime, API) VALUES (#{l}, #{Time.now.to_i}, 0, '{}')"
end
File.new('marketable.json','w+').write(rest_api.body.to_s)

i = CSV.open('Recipe.csv')

i.each do |l|
	if l[4].to_i != 0 then
		t[l[4].to_i][3].push [l[5].to_i,
			[l[6] .to_i,  l[7].to_i],
			[l[8] .to_i,  l[9].to_i],
			[l[10].to_i, l[11].to_i],
			[l[12].to_i, l[13].to_i],
			[l[14].to_i, l[15].to_i],
			[l[16].to_i, l[17].to_i],
			[l[18].to_i, l[19].to_i],
			[l[20].to_i, l[21].to_i],
			[l[22].to_i, l[23].to_i],
			[l[24].to_i, l[25].to_i]
		]
	end
end

i.close

i = File.new('data.json', 'w+')
js = JSON.fast_generate(t)
i.write js
i.close


db.execute 'DELETE FROM Item'
db.execute 'DELETE FROM CreatTable'
db.execute 'DELETE FROM sqlite_sequence WHERE name = \'CreatTable\''


t.each do |l|
	#db.execute 'INSERT INTO Item (ID, Name, TradeAble, Universalis, BuyPrice, SellPrice) VALUES (0,"N",0,0,0,0)'
	#break
	String ts = "INSERT INTO Item (ID, Name, TradeAble, Universalis, BuyPrice, SellPrice) VALUES (#{l[0].to_s},#{'"'+l[1].to_s+'"'},#{(l[2][0]?1:0).to_s},#{(l[2][3]?1:0).to_s},#{l[2][1].to_s},#{l[2][2].to_s})"
	db.execute ts
	if l[3].class == Array then
		l[3].each do |r|
			String ts = "INSERT INTO CreatTable (Id, Count, I0, A0, I1, A1, I2, A2, I3, A3, I4, A4, I5, A5, I6, A6, I7, A7, I8, A8, I9, A9) VALUES (#{l[0].to_s},#{r.join(',')})"
			db.execute ts
		end
	end
end

db.execute 'DELETE FROM CreatTable WHERE rowid NOT IN (SELECT MIN(rowid) FROM CreatTable GROUP BY Id, Count, I0, A0, I1, A1, I2, A2, I3, A3, I4, A4, I5, A5, I6, A6, I7, A7)'
