require 'json'
require 'rest-client'
require 'sqlite3'
require 'zlib'
require 'base64'
$universalis_item_cut = Array.new ['dcName', 'listings', 'itemID', 'averagePrice', 'currentAveragePrice', 'lastUploadTime', 'lastReviewTime', 'pricePerUnit', 'total', 'worldName', 'recentHistory', 'quantity']

##
# Universalis
# 对单个物品数据进行处理的类
class Universalis

  attr_accessor :id, :last_update, :req

  #物品 ID
  @id=0

  #物品上次更新时间
  @last_update

  #物品连接的API介面
  @api

  #物品当前的查询结果
  @req

  @@db =

  def initialize(id, api)
    @id = id
    @api = api
  end
  def execute
    @api.execute
  end
  def db
    return @@db
  end
end


##
# UniversalisAPI
# UniverSalis 网站的API接口功能抽象

class UniversalisAPI

  #用以处理物品请求的缓存
  @item_cache

  #应以处理物品返回值的缓存
  @req_cache

  #上次更新时间
  @link_time

  #通用数据库
  @@db

  #id过滤
  @@id_table
  def initialize(db)
    @item_cache = Array.new
    @req_cache = Hash.new
    @link_time = 0
    @@db = db

    @@id_table = JSON.parse(RestClient.get('https://universalis.app/api/v2/marketable').body.to_s).to_a
  end

  #请求物品销售数据
  def current_data(s='陆行鸟', *i)
    req = RestClient.get("https://universalis.app/api/v2/#{URI::encode_uri_component(s)}/#{i.join(',')}?listings=100&entries=100")
    @link_time = Time.now.to_i
    return req.body
  end

  ##
  # 执行批量运行
  # 使用 cadd 将id写入列表
  # 使用 cexec 进行联网执行
  # 使用 cdb 写入数据库

  def cadd(id)
    @item_cache.push(id) if @@id_table.include? id
    p @item_cache
  end
  def cexec(s='陆行鸟')
    h = JSON.parse(self.current_data(s, @item_cache))
    h['items'].map do |key, value|
      value.reject! do |k, v|
        !($universalis_item_cut.include?(k))
      end
      value['listings'].each do |v|
        v.reject! do |k, v|
         !($universalis_item_cut.include?(k))
        end
      end
      value['recentHistory'].each do |v|
        v.reject! do |k, v|
         !($universalis_item_cut.include?(k))
        end
      end
      @req_cache[key.to_i] = value
    end
    @req_cache.rehash
  end

  def _enc(value)
    return Base64::encode64 Zlib::Deflate.deflate(value.to_json)
  end
  def _dec(value)
    return JSON::parse Zlib::Inflate.inflate(Base64::decode64(value))
  end

  def cdb()
    @req_cache.rehash
    @req_cache.each do |key, value|
      ts = "UPDATE universalis SET UPDATETIME = #{Time.now.to_i} , LASTUPLOADTIME = #{value['lastUploadTime']/1000}, API = \"#{self._enc value}\" WHERE ID = #{key} AND LASTUPLOADTIME < #{value['lastUploadTime']/1000}"
      #p ts
      @@db.execute ts
    end
  end
  def cget(id)
    @req_cache.rehash
    return @req_cache[id] if @req_cache.key?(id)
  end

end

a = UniversalisAPI.new SQLite3::Database.open('data.db')
